// require
var express = require("express");
var router = express.Router();
var Register = require("../model/register");



module.exports = {
  all: (req, res) => {
    Register.find().exec((err, registers) => {
      if (err) {
        res.status(500).send(err);
      } else {
        res.status(200).send(registers);
      }
    });
  },
  create: (req, res) => {
    let regi = new Register({
      title: req.body.title,
      content: req.body.content,
      file: req.body.file,
    });

    regi.save((err, register) => {
      if (err) {
        res.status(500).send(err);
      } else {
        res.status(200).send(register);
      }
    });
  },
  update: (req, res) => {
    let id = req.params.id;

    Register.findById(id, (err, regi) => {
      if (err) {
        res.status(500).send(err);
      } else if (!regi) {
        res.status(404).send({});
      } else {
        regi.title = req.body.title;
        regi.content = req.body.content;
        regi.file = req.body.file;
        regi
          .save()
          .then((newRegi) => res.status(200).send(newRegi))
          .catch((e) => {
            res.status(500).send(e);
          });
      }
    });
  },
  delete: async (req, res) => {
    try {
      let id = req.params.id;
      await Register.deleteOne({ _id: id });
      res.status(200).send({});
    } catch (error) {
      res.status(500).send(error);
    }
  },
};

// router.get("/", (req, res) => {
//   Register.find().exec((err, registers) => {
//     if (err) {
//       res.status(500).send(err);
//     } else {
//       res.status(200).send(registers);
//     }
//   });
// });

// router.post("/", (req, res) => {
//   let regi = new Register({
//     title: req.body.title,
//     content: req.body.content,
//   });

//   regi.save((err, register) => {
//     if (err) {
//       res.status(500).send(err);
//     } else {
//       res.status(200).send(register);
//     }
//   });
// });

// router.patch("/:id", (req, res) => {
//   let id = req.params.id;

//   Register.findById(id, (err, regi) => {
//     if (err) {
//       res.status(500).send(err);
//     } else if (!regi) {
//       res.status(404).send({});
//     } else {
//       regi.title = req.body.title;
//       regi.content = req.body.content;
//       regi
//         .save()
//         .then((newRegi) => res.status(200).send(newRegi))
//         .catch((e) => {
//           res.status(500).send(e);
//         });
//     }
//   });
// });

// router.delete("/:id", async (req, res) => {
//   try {
//     let id = req.params.id;
//     await Register.deleteOne({ _id: id });
//     res.status(200).send({});
//   } catch (error) {
//     res.status(500).send(error);
//   }
// });
