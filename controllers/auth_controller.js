const User = require("../model/user");
const bcrypt = require("bcryptjs");
const consts = require("../consts/consts");
const jwt = require("jsonwebtoken");

module.exports = {
  signup: async (req, res) => {
    try {
      let u = await User.findOne({ email: req.body.email });
      if (!u) {
        const user = new User(req.body);
        user.password = bcrypt.hashSync(req.body.password, consts.bcryptSalts); // Criptografar senha
        await user.save();
        delete user.password; // Usado para não retornar a senha na requisição para o cliente
        res.status(200).json(user);
      } else {
        res.status(403).json({ message: "Email já cadastrado", error: {} });
      }
    } catch (error) {
      res
        .status(500)
        .json({ message: "Erro ao Tentar Cadastrar Usuário!", error: error });
    }
  },
  login: (req, res) => {
    const password = req.body.password;
    const email = req.body.email;
    User.findOne({ email: email })
      .lean()
      .exec((err, user) => {
        if (err) {
          return res
            .status(500)
            .json({ message: "Erro no servidor", error: err });
        }

        const auth_err = password == "" || password == null || !user; // verifica se ocorreu um erro

        if (!auth_err) {
          if (bcrypt.compareSync(password, user.password)) {
            let token = jwt.sign({ _id: user._id }, consts.keyJwt, {
              expiresIn: consts.expiresJwt,
            });

            delete user.password; // para não retornar a senha

            return res.json({
              ...user,
              token: token,
            });
          }
        }
        return res.status(404).json({ message: "Email ou senha incorretos!" });
      });
  },
};
