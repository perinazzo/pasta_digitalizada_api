// require
var express = require("express");
var router = express.Router();
var registerController = require("../controllers/register_controller");


router.get("/registers", registerController.all);
router.post("/registers", registerController.create);
router.patch("/registers/:id", registerController.update);
router.delete("/registers/:id", registerController.delete);

module.exports = router;
