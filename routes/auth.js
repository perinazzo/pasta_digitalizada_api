// require
var express = require("express");
var router = express.Router();
var authController = require('../controllers/auth_controller');

router.post("/login", authController.login);
router.post("/signup", authController.signup)

module.exports = router;