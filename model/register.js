//required

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var registerSchema = new Schema(
  {
    title: String,
    content: String,
    file: String
  },
  { versionKey: false }
);

module.exports = mongoose.model("Register", registerSchema);