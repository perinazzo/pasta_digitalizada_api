// requires
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const app = express();
const api = require("./routes/api");
const auth = require("./routes/auth");

//middleware
app.use(bodyParser.json({limit: '1mb'}));
app.use(bodyParser.urlencoded({ extended: true}));
app.use(cors());

mongoose.connect("mongodb://localhost:27017/digital_registers", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.use("/api", api);
app.use("/auth", auth);
app.use((req, res, next) => {
  res.status(404).send("Não encontrado");
});

app.listen(3000);
